package com.worldofautomation.tests;

import com.worldofautomation.core.TestBase;

public class EventCase extends TestBase {

    public void performAction(Events events) {
        switch (events) {
            case sleep:
                waitFor(5);
                break;
            case clickOnReturnsAndOrders:
                homePage.clickOnReturnsAndOrders();
                break;

            case validateSignInPage:
                signInPage.validateSignInPageLoaded();
                break;

            case validateContinueButtonClicked:
                signInPage.clickOnContinueButton();
                break;

            case validateErrorMessage:
                signInPage.validateErrorMessage();
                break;
        }

    }

    public enum Events {
        clickOnReturnsAndOrders, validateSignInPage, validateContinueButtonClicked, validateErrorMessage, sleep
    }
}
