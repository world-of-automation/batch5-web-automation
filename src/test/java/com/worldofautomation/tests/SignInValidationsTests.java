package com.worldofautomation.tests;

import com.worldofautomation.core.TestBase;
import org.testng.annotations.Test;

public class SignInValidationsTests extends TestBase {

    @Test(groups = "regression", enabled = false)
    public void validateUserCanSeeErrorMessageTryingToLoginWithoutPhone1() {
        homePage.clickOnReturnsAndOrders();
        signInPage.validateSignInPageLoaded();
        signInPage.clickOnContinueButton();
        signInPage.validateErrorMessage();
        waitFor(5);
    }

    @Test(groups = "regression", enabled = false)
    public void validateUserCanSeeErrorMessageTryingToLoginWithoutPhone2() {
        homePage.clickOnReturnsAndOrders();
        signInPage.validateSignInPageLoaded();
        signInPage.clickOnContinueButton();
        signInPage.validateErrorMessage();
        waitFor(5);
    }

    @Test(groups = "debug", enabled = false)
    public void validateUserCanSeeErrorMessageTryingToLoginWithoutPhone3() {
        homePage.clickOnReturnsAndOrders();
        signInPage.validateSignInPageLoaded();
        signInPage.clickOnContinueButton();
        signInPage.validateErrorMessage();
        waitFor(5);
    }


    @Test(dataProvider = "dataForLogin", dataProviderClass = DataProvidersForTests.class)
    public void validateUserCanLoginWithDifferentPhoneNumbers(String username, int password) {
        homePage.clickOnSignInButton();

        signInPage.typeOnEmailField(username);

        signInPage.clickOnContinueButton();
        signInPage.typeOnPasswordField(password);

    }


}
