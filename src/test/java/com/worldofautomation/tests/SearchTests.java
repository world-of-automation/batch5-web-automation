package com.worldofautomation.tests;

import com.worldofautomation.core.TestBase;
import org.testng.annotations.Test;

public class SearchTests extends TestBase {


    // test driven --> TDD
    // data driven -- > DDD
    // keywords driven --> KDD
    // behaviour driven --> BDD

    @Test(enabled = false)
    public void validateUserCanSearchForItems() {

        // pre-requisite :
        // create a table in db with 2 columns
        // one column with 10 test data to search in amazon
        // another column will be empty

        // Test
        // get the list of data to search --> get from db using DBConnection class
        // iterate through the list, and search one by one
        // get the total number of total records found for that specific item in amazon
        // store those numbers in the second column in the db
    }

    @Test(dataProvider = "dataForSearch", dataProviderClass = DataProvidersForTests.class)
    public void validateUserCanSearchForDifferentItems(String data) {
        homePage.typeOnSearchBar(data);
        homePage.clickOnSearchButton();
        homePage.validateSearchItemDisplayed();
    }

}
