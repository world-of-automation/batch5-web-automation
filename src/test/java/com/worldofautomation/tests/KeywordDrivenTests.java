package com.worldofautomation.tests;

import org.testng.annotations.Test;

public class KeywordDrivenTests extends EventCase {

    @Test(groups = "regression")
    public void validateUserCanSeeErrorMessageTryingToLoginWithoutPhone1() {
        performAction(Events.clickOnReturnsAndOrders);
        performAction(Events.validateSignInPage);
        performAction(Events.validateContinueButtonClicked);
        performAction(Events.validateErrorMessage);
        performAction(Events.sleep);

    }
}
