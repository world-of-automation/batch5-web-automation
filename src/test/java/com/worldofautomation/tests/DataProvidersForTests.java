package com.worldofautomation.tests;

import org.testng.annotations.DataProvider;

public class DataProvidersForTests {

    // homework : store credentials in the properties file
    // and inject them via data providers in test

    @DataProvider
    public Object[][] dataForLogin() {
        // write the code to read from properties file
        return new Object[][]{{"9293739212", 1111}, {"3478631519", 1213}};
    }

    @DataProvider
    public Object[][] dataForSearch() {
        return new Object[][]{{"Java Books"}, {"Selenium Books"}};
    }
}
