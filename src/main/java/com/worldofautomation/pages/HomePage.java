package com.worldofautomation.pages;

import com.worldofautomation.core.ExtentTestManager;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.testng.Assert;

public class HomePage {

    @FindBy(id = "nav-orders")
    private WebElement returnsAndOrders;

    @FindBy(linkText = "register")
    private WebElement registerButton;

    @FindBy(id = "twotabsearchtextbox")
    private WebElement searchBar;

    @FindBy(id = "nav-search-submit-button")
    private WebElement searchButton;

    @FindBy(xpath = "//div[@class='a-section a-spacing-small a-spacing-top-small']")
    private WebElement searchResult;

    @FindBy(id = "nav-link-accountList-nav-line-1")
    private WebElement signInButton;

    public void clickOnRegisterButton() {
        registerButton.click();
    }

    public void clickOnReturnsAndOrders() {
        returnsAndOrders.click();
        ExtentTestManager.log("ReturnsAndOrders has been clicked");
    }


    public void typeOnSearchBar(String data) {
        searchBar.clear();
        searchBar.sendKeys(data);
        ExtentTestManager.log("Typed on search bar with data : " + data);
    }

    public void clickOnSearchButton() {
        searchButton.click();
        ExtentTestManager.log("searchButton has been clicked");
    }


    public void validateSearchItemDisplayed() {
        Assert.assertTrue(searchResult.isDisplayed());
        ExtentTestManager.log("searchResult is displayed as expected");
    }


    public void clickOnSignInButton() {
        signInButton.click();
    }

}
