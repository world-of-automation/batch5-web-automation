package com.worldofautomation.pages;

import com.worldofautomation.core.ExtentTestManager;
import com.worldofautomation.core.TestBase;
import org.apache.log4j.Logger;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.testng.Assert;

public class SignInPage {

    private static final Logger LOGGER = Logger.getLogger(SignInPage.class);

    @FindBy(xpath = "//h1[@class='a-spacing-small']")
    private WebElement signInText;

    @FindBy(id = "continue")
    private WebElement continueButton;

    @FindBy(xpath = "(//div[@class='a-alert-content'])[2]")
    private WebElement errorMessage;

    @FindBy(id = "ap_email")
    private WebElement emailField;

    @FindBy(id = "ap_password")
    private WebElement passwordField;


    /*public void validateVerifyTextIsDisplayedAndAsExpected() {
        boolean answer = verifyText.isDisplayed();
        Assert.assertTrue(answer);
        ExtentTestManager.log("VerifyText is displayed :" + answer);

        String data = verifyText.getText();
        Assert.assertEquals(data, "Please verify yourself to continue");
        ExtentTestManager.log("VerifyText's data is :" + data);
    }*/


    public void validateSignInPageLoaded() {
        String currentUrl = TestBase.driver.getCurrentUrl();
        Assert.assertTrue(currentUrl.contains("signin"));
        ExtentTestManager.log("Current url contains Sign In");
        LOGGER.info("Something i want to see inconsole" + currentUrl);

        String message = signInText.getText();
        Assert.assertTrue(signInText.isDisplayed());
        Assert.assertEquals("Sign-In", message, "Sign-In wasn't correctly displayed");
        ExtentTestManager.log(message + " is displayed and matches as expected");
    }


    public void clickOnContinueButton() {
        TestBase.waitTillClickable(continueButton);
        continueButton.click();
        ExtentTestManager.log("Continue button has been clicked");

    }

    public void validateErrorMessage() {
        Assert.assertTrue(errorMessage.isDisplayed());
        Assert.assertEquals(errorMessage.getText(), "Enter your email or mobile phone number");
        ExtentTestManager.log("Error message is displayed and validated ");
    }

    public void typeOnEmailField(String data) {
        emailField.sendKeys(data);
    }

    public void typeOnPasswordField(int password) {
        passwordField.sendKeys(String.valueOf(password));
    }
}
