package com.worldofautomation.basic;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.Test;

import java.time.Duration;
import java.util.List;

public class EbayTests extends CommonAPI {


    @Test
    public void waitExample() {
        //explicit wait
        WebElement element = driver.findElement(By.id("gh-ac"));

        WebDriverWait webDriverWait = new WebDriverWait(driver, Duration.ofSeconds(10));
        webDriverWait.until(ExpectedConditions.elementToBeClickable(element));
        webDriverWait.until(ExpectedConditions.visibilityOf(element));
    }


    @Test
    public void dropDownTest() {
        setupBrowser("chrome", "https://www.ebay.com");

        driver.findElement(By.id("gh-ac")).sendKeys("Java Books");

        //getting all values from a dropdown
        List<WebElement> categories = driver.findElements(By.xpath("//select[@id='gh-cat']/option"));

        for (int i = 0; i < categories.size(); i++) {
            System.out.println(categories.get(i).getText());
        }

        WebElement categorySelector = driver.findElement(By.xpath("//select[@id='gh-cat']"));

        Select select = new Select(categorySelector);
        select.selectByIndex(4);
        waitFor(5);

        select.selectByVisibleText("Art");
        waitFor(5);

        quitChrome();
    }


    @Test
    public void mouseHoverTest() {
        setupBrowser("chrome", "https://www.ebay.com");

        WebElement motors = driver.findElement(By.linkText("Motors"));

        Actions actions = new Actions(driver);
        actions.moveToElement(motors).build().perform();

        waitFor(5);

        driver.findElement(By.linkText("Cars & Trucks")).click();

        waitFor(5);

        quitChrome();
    }


    @Test
    public void scrollDownToNewsTest() {
        setupBrowser("chrome", "https://www.ebay.com");

        WebElement news = driver.findElement(By.linkText("News"));

        JavascriptExecutor js = (JavascriptExecutor) driver;
        js.executeScript("arguments[0].scrollIntoView(true);", news);
        waitFor(5);

        quitChrome();
    }

    @Test
    public void scrollDownToFacebookTest() {
        setupBrowser("chrome", "https://www.ebay.com");

        WebElement facebook = driver.findElement(By.linkText("Facebook"));

        scrollDownToSpecificElement(facebook);
        waitFor(5);

        quitChrome();
    }

    @Test
    public void scrollDownTest() {
        setupBrowser("chrome", "https://www.ebay.com");

        JavascriptExecutor js = (JavascriptExecutor) driver;
        js.executeScript("window.scrollBy(0,1000)");
        waitFor(5);

        js.executeScript("window.scrollBy(0,1000)");
        waitFor(5);

        quitChrome();
    }

    @Test
    public void dragNDropTest() {
        setupBrowser("chrome", "https://demo.guru99.com/test/drag_drop.html");
        WebElement from = driver.findElement(By.xpath("(//a[@class='button button-orange'])[2]"));
        WebElement to = driver.findElement(By.id("amt7"));

        Actions actions = new Actions(driver);

        actions.dragAndDrop(from, to).build().perform();

        waitFor(5);

        quitChrome();
    }


    @Test
    public void alertTest() {
        setupBrowser("chrome", "https://demo.guru99.com/test/delete_customer.php");
        driver.findElement(By.xpath("//input[@name='cusid']")).sendKeys("1");
        driver.findElement(By.xpath("//input[@name='submit']")).click();

        waitFor(2);

        driver.switchTo().alert().accept();

        String alertData = driver.switchTo().alert().getText();

        System.out.println(alertData);

        //driver.switchTo().alert().sendKeys("");

        waitFor(5);

        quitChrome();
    }

    @Test
    public void clicksTest() {
        Actions actions = new Actions(driver);
        WebElement element = driver.findElement(By.xpath(""));
        actions.doubleClick(element).build().perform();

        actions.contextClick(element).build().perform();
    }


    @Test
    public void iFrameTest() {
        setupBrowser("chrome", "https://demoqa.com/frames");
        JavascriptExecutor js = (JavascriptExecutor) driver;
        js.executeScript("window.scrollBy(0,1000)");

        waitFor(5);

        driver.switchTo().frame("frame2");
        js.executeScript("window.scrollBy(0,1000)");


        driver.switchTo().defaultContent();
        waitFor(5);
        quitChrome();
    }


    @Test
    public void radioButton() {
        setupBrowser("chrome", "https://www.aa.com/");
        waitFor(2);

        // from the page click on the one way if that's not selected
        List<WebElement> data = driver.findElements(By.xpath("//input[@type='radio']"));

        for (WebElement datum : data) {
            String value = datum.getAttribute("value");
            if (value.equalsIgnoreCase("oneway")) {

                System.out.println(datum.isEnabled());

                System.out.println(datum.isSelected());
                driver.findElement(By.xpath("//label[@for='flightSearchForm.tripType.oneWay']")).click();
                System.out.println(datum.isSelected());
                break;
            }
        }


        waitFor(5);
        quitChrome();
    }


}
