package com.worldofautomation.basic;

import org.openqa.selenium.By;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.Test;

public class BrowserLaunch2 {
    private ChromeDriver driver;

    public void setupChrome(String url) {
        System.setProperty("webdriver.chrome.driver", "src/main/resources/chromedriver");
        driver = new ChromeDriver();
        driver.get(url);
    }

    public void quitChrome() {
        driver.quit();
    }

    public void waitFor(int seconds) {
        try {
            Thread.sleep(seconds * 1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void openChromeAndNavigateToEbay() {
        setupChrome("https://www.ebay.com");
        waitFor(5);
        quitChrome();
    }


    @Test
    public void searchForJavaBooksInEbay() throws InterruptedException {
        setupChrome("https://www.ebay.com");

        driver.findElement(By.id("gh-ac")).sendKeys("Java books");

        driver.findElement(By.id("gh-btn")).click();

        waitFor(5);
        quitChrome();
    }


    @Test
    public void navigateToAmazon() {
        setupChrome("https://www.amazon.com");
        waitFor(5);
        quitChrome();
    }


}
