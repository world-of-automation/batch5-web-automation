package com.worldofautomation.basic;

import org.openqa.selenium.By;
import org.testng.annotations.Test;

public class SeleniumTests extends CommonAPI {


    @Test
    public void openBrowserAndNavigateToEbay() {
        setupBrowser("chrome", "https://www.ebay.com");
        waitFor(5);
        quitChrome();
    }


    @Test
    public void searchForJavaBooksInEbay() {
        setupBrowser("chrome", "https://www.ebay.com");

        driver.findElement(By.id("gh-ac")).sendKeys("Java books");

        driver.findElement(By.id("gh-btn")).click();
        waitFor(5);
        quitChrome();
    }


    @Test
    public void navigateToAmazon() {
        setupBrowser("firefox", "https://www.amazon.com");
        waitFor(5);
        quitChrome();
    }


    @Test
    public void clickOnRegisterButtonOnEbay() {
        setupBrowser("chrome", "https://www.ebay.com");
        driver.findElement(By.linkText("register")).click();
        waitFor(5);
        quitChrome();
    }

    // id
    // linktext
    //partial link text

    @Test
    public void clickOnVerifiedRightsButtonOnEbay() {
        setupBrowser("chrome", "https://www.ebay.com");
        driver.findElement(By.partialLinkText("Verified Rights")).click();
        waitFor(5);
        quitChrome();
    }


    // xpath == extended html path

    //select[@aria-label='Select a category for search']

    // //TagName [@key='value']


    @Test
    public void clickOnDropDownButtonOnEbay() {
        setupBrowser("chrome", "https://www.ebay.com");
        driver.findElement(By.xpath("//select[@aria-label='Select a category for search']")).click();
        waitFor(5);
        quitChrome();
    }


    @Test
    public void clickOnDropDownShopByCategoryButtonOnEbay() {
        setupBrowser("chrome", "https://www.ebay.com");
        driver.findElement(By.xpath("//button[@id='gh-shop-a']")).click();
        waitFor(5);
        quitChrome();
    }


}
