package com.worldofautomation.basic;

import org.openqa.selenium.By;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.Test;

public class BrowserLaunch {

    public static void main(String[] args) throws InterruptedException {
        System.setProperty("webdriver.chrome.driver", "src/main/resources/chromedriver");
        ChromeDriver driver = new ChromeDriver();
        driver.get("https://www.ebay.com");
        Thread.sleep(5000);
        driver.quit();
    }


    @Test
    public void openChromeAndNavigateToEbay() throws InterruptedException {
        System.setProperty("webdriver.chrome.driver", "src/main/resources/chromedriver");
        ChromeDriver driver = new ChromeDriver();
        driver.get("https://www.ebay.com");
        Thread.sleep(5000);
        driver.quit();
    }


    @Test
    public void searchForJavaBooksInEbay() throws InterruptedException {
        System.setProperty("webdriver.chrome.driver", "src/main/resources/chromedriver");
        ChromeDriver driver = new ChromeDriver();
        driver.get("https://www.ebay.com");

        driver.findElement(By.id("gh-ac")).sendKeys("Java books");

        driver.findElement(By.id("gh-btn")).click();

        Thread.sleep(5000);
        driver.quit();
    }


    @Test
    public void launchFireFox() throws InterruptedException {
        System.setProperty("webdriver.gecko.driver", "src/main/resources/geckodriver");
        FirefoxDriver driver = new FirefoxDriver();
        driver.get("https://www.ebay.com");
        Thread.sleep(5000);
        driver.quit();
    }


}
