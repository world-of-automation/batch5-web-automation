package com.worldofautomation.basic;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.Test;

public class BrowserLaunch3 {
    private WebDriver driver;

    public void setupBrowser(String browserName, String url) {
        if (browserName.equalsIgnoreCase("chrome")) {
            System.setProperty("webdriver.chrome.driver", "src/main/resources/chromedriver");
            driver = new ChromeDriver();
        } else {
            System.setProperty("webdriver.gecko.driver", "src/main/resources/geckodriver");
            driver = new FirefoxDriver();
        }
        driver.get(url);
    }

    public void quitChrome() {
        driver.quit();
    }

    public void waitFor(int seconds) {
        try {
            Thread.sleep(seconds * 1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void openBrowserAndNavigateToEbay() {
        setupBrowser("chrome", "https://www.ebay.com");
        waitFor(5);
        quitChrome();
    }


    @Test
    public void searchForJavaBooksInEbay() throws InterruptedException {
        setupBrowser("chrome", "https://www.ebay.com");

        driver.findElement(By.id("gh-ac")).sendKeys("Java books");

        driver.findElement(By.id("gh-btn")).click();
        waitFor(5);
        quitChrome();
    }


    @Test
    public void navigateToAmazon() {
        setupBrowser("firefox", "https://www.amazon.com");
        waitFor(5);
        quitChrome();
    }


}
